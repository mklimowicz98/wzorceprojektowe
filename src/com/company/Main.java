package com.company;

import com.company.zadanie3.*;

public class Main {

    public static void main(String[] args) {
//        Zadanie1
//        Pizza pizza = new Pizza.Builder("duży", "śmietanowy", true).papryka().kielbasa().szynka().sosWewnatrzCiasta().build();
//        pizza.PodanoDoStolu();

//        Zadanie2
//        House house = new House();
//        house.enter();
//        house.leave();

//        LightControlInterface gate = new EntryGateLightDecorator(new Lights());
//        LightControlInterface garden = new GardenLightDecorator(new Lights());
//        LightControlInterface homeCinema = new HomeCinemaLightDecorator(new Lights());
//        homeCinema.turnOn();

//        Zadanie3
        Restaurant restaurant = new Restaurant();
        Kitchen kitchen = new Kitchen();
        restaurant.registerObserver(kitchen);

        MakeOrderCommand order = new MakeOrderCommand(new Order());
        order.setStrategy(new DiscountOrderStrategy());
        order.execute();
        order.calculateDiscount();
        restaurant.addOrder(order);
    }
}
