package com.company.zadanie3;

public interface Subject {
    public void registerObserver(Kitchen kitchen);
    public void notifyObserver();
}
