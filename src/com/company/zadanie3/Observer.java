package com.company.zadanie3;

public interface Observer {
    public void update();
}
