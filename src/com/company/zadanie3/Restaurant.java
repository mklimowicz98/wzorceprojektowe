package com.company.zadanie3;

import java.util.ArrayList;
import java.util.List;

public class Restaurant implements Subject{
    private List<Command> orders;
    private List<Kitchen> observers;

    public Restaurant(){
        orders = new ArrayList<>();
        observers = new ArrayList<>();
    }

    public void addOrder(Command order){
        this.orders.add(order);
        notifyObserver();
    }

    @Override
    public void registerObserver(Kitchen kitchen){
        System.out.println("Observer registered");
        observers.add(kitchen);
    }

    @Override
    public void notifyObserver(){
        for (Kitchen k: observers) {
            k.update();
        }
    }


}
