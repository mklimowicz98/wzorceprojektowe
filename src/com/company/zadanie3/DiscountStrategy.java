package com.company.zadanie3;

public interface DiscountStrategy {
    void calculateDiscount(Order order);
}
