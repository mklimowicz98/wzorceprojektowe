package com.company.zadanie3;

@FunctionalInterface
public interface Command {
    void execute();
}
