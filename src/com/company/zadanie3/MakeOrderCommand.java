package com.company.zadanie3;

public class MakeOrderCommand implements Command{
    private Order order;
    private DiscountStrategy strategy;

    public MakeOrderCommand(Order order){
        super();
        this.order=order;
    }

    public void execute(){
        order.createOrder();
    }

    public void setStrategy(DiscountStrategy strategy){
        this.strategy=strategy;
    }

    public void calculateDiscount(){
        this.strategy.calculateDiscount(this.order);
    }
}
