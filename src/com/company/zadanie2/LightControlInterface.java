package com.company.zadanie2;

public interface LightControlInterface {
    void turnOff();
    void turnOn();
}
