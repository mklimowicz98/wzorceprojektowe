package com.company.zadanie2;

public class EntryGateLightDecorator extends LightDecorator {
    public  EntryGateLightDecorator(LightControlInterface decoratedLight){
        super(decoratedLight);
    }

    @Override
    public void turnOn(){
        decoratedLight.turnOn();
        gateLightOn();
    }

    @Override
    public void turnOff(){
        decoratedLight.turnOff();
        gateLightOff();
    }

    private void gateLightOn(){
        System.out.println("Entry gate light on");
    }

    private void gateLightOff(){
        System.out.println("Entry gate light off");
    }

}
