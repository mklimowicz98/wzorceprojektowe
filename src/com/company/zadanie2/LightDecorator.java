package com.company.zadanie2;

public class LightDecorator implements LightControlInterface {
    protected LightControlInterface decoratedLight;

    public LightDecorator(LightControlInterface decoratedLight){
        this.decoratedLight=decoratedLight;
    }

    public void turnOn(){
        decoratedLight.turnOn();
    }

    public void turnOff(){
        decoratedLight.turnOff();
    }
}
