package com.company.zadanie2;

public interface HeatingControlInterface {
    void turnOn();
    void turnOff();
    void setTemperature(int temperatue);
}
