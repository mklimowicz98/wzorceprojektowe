package com.company.zadanie2;

public class HomeCinemaLightDecorator extends LightDecorator{
    public HomeCinemaLightDecorator(LightControlInterface decoratedLight){
        super(decoratedLight);
    }

    @Override
    public void turnOn(){
        decoratedLight.turnOn();
        setColor();
        dynamicLight();
    }

    private void setColor(){
        System.out.println("Red color set");
    }

    private void dynamicLight(){
        System.out.println("Dynamic Light set");
    }
}
