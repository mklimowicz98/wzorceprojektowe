package com.company.zadanie2;

public class GardenLightDecorator extends LightDecorator {
    public GardenLightDecorator(LightControlInterface decoratedLight){
        super(decoratedLight);
    }

    @Override
    public void turnOn(){
        decoratedLight.turnOn();
        dim();
    }

    private void dim(){
        System.out.println("50% efficiency set");
    }
}
