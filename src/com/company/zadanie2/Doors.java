package com.company.zadanie2;

public class Doors implements DoorControlInterface {
    public void open(){
        System.out.println("Doors open");
    }
    public void close(){
        System.out.println("Doors close");
    }
    public void lock(){
        System.out.println("Doors lock");
    }
    public void unlock(){
        System.out.println("Doors unlock");
    }
}
