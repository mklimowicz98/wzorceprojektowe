package com.company.zadanie2;

public interface DoorControlInterface {
    void open();
    void close();
    void lock();
    void unlock();
}
