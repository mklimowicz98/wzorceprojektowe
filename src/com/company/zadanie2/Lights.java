package com.company.zadanie2;

public class Lights implements LightControlInterface {
    public void turnOn(){
        System.out.println("Lights turn on");
    }

    public void turnOff(){
        System.out.println("Lights turn off");
    }
}
