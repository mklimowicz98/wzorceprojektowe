package com.company.zadanie2;

public class House {
    Doors doors = new Doors();
    Heating heating = new Heating();
    Lights lights = new Lights();

    public void enter(){
        doors.unlock();
        doors.open();
        heating.turnOn();
        heating.setTemperature(25);
        lights.turnOn();
    }

    public void leave(){
        heating.turnOff();
        lights.turnOff();
        doors.close();
        doors.lock();
    }
}
