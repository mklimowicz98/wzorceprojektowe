package com.company.zadanie1;

public class Pizza {
    private String rozmiar;
    private String sos;
    private boolean calzone;
    private boolean sosWewnatrzCiasta;
    private boolean szynka;
    private boolean pieczarki;
    private boolean cebula;
    private boolean papryka;
    private boolean kielbasa;
    private boolean ser;

    public static class Builder {
        //Wymagane
        private String rozmiar = "Średna";
        private String sos;
        private boolean calzone = false;
        private boolean sosWewnatrzCiasta = false;
        //Opcjonalne
        private boolean szynka = false;
        private boolean pieczarki = false;
        private boolean cebula = false;
        private boolean papryka = false;
        private boolean kielbasa = false;
        private boolean ser = false;

        public Builder(String rozmiar, String sos, boolean calzone) {
            if (rozmiar != "duży" && rozmiar != "średni" && rozmiar != "mały") {
                System.out.println("Podano zły rozmiar. Ustawiono rozmiar domyślny: ŚREDNI");
            }
            if (sos != "pomidorowy" && sos != "śmietanowy") {
                System.out.println("Podano zły sos. Ustawiono sos domyślny: POMIDOROWY");
            }
            this.rozmiar = rozmiar;
            this.sos = sos;
            this.calzone = calzone;
        }

        public Builder szynka() {
            this.szynka = true;
            return this;
        }

        public Builder pieczarki() {
            this.pieczarki = true;
            return this;
        }

        public Builder cebula() {
            this.cebula = true;
            return this;
        }

        public Builder papryka() {
            this.papryka = true;
            return this;
        }

        public Builder kielbasa() {
            this.kielbasa = true;
            return this;
        }

        public Builder ser() {
            this.ser = true;
            return this;
        }

        public Builder sosWewnatrzCiasta() {
            if (this.calzone == true) {
                this.sosWewnatrzCiasta = true;
            } else {
                System.out.println("Pizza nie jest calzone. Nie można umieścić sosu wewnątrz ciasta");
            }

            return this;

        }

        public Pizza build() {
            return new Pizza(this);
        }
    }

    public Pizza(Builder builder) {
        this.rozmiar = builder.rozmiar;
        this.sos = builder.sos;
        this.szynka = builder.szynka;
        this.pieczarki = builder.pieczarki;
        this.cebula = builder.cebula;
        this.papryka = builder.papryka;
        this.kielbasa = builder.kielbasa;
        this.ser = builder.ser;
        this.calzone = builder.calzone;
        this.sosWewnatrzCiasta = builder.sosWewnatrzCiasta;
    }

    public void podanoDoStolu() {
        System.out.println("Calzone: " + this.calzone);
        System.out.println("Rozmiar: " + this.rozmiar);
        System.out.println("Sos: " + this.sos);
        System.out.println("Sos wewnątrz ciasta: " + this.sosWewnatrzCiasta);
        System.out.println("szynka: " + this.szynka);
        System.out.println("pieczarki: " + this.pieczarki);
        System.out.println("cebula: " + this.cebula);
        System.out.println("papryka: " + this.papryka);
        System.out.println("kielbasa: " + this.kielbasa);
        System.out.println("ser: " + this.ser);
    }
}
